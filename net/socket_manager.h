//
// Created by Yo on 16/4/21.
//

#ifndef BILAI_SUITE_SOCKET_MANAGER_H
#define BILAI_SUITE_SOCKET_MANAGER_H

#include <QObject>
#include <QTcpSocket>
#include <QSslSocket>
#include <QEventLoop>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include "../logs.h"

class SocketManager : public QObject {

Q_OBJECT

private:
    QTcpSocket *$tcpSocket;
    QSslSocket *$sslSocket;

    QString $host;
    int $port;
    bool $isSSL;

public:
    explicit SocketManager(QObject *parent = nullptr, const QString &host = "", int port = 0, bool isSSL = false);

    ~SocketManager() override {
        this->getSocket()->close();
        delete this->getSocket();
    }

    // getters & setters

    QAbstractSocket *getSocket() const;

    QString host();

    int port() const;

    bool isSSL() const;

    // end getters & setters

    // returns http body
    QString get(const QString &path);

private:
    void connectCheck();
};


#endif //BILAI_SUITE_SOCKET_MANAGER_H
