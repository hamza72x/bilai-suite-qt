//
// Created by Yo on 16/4/21.
//

#ifndef BILAI_SUITE_NET_CPP
#define BILAI_SUITE_NET_CPP

#include <QObject>
#include "socket_manager.h"
#include <QJsonDocument>

class Net : public QObject {

Q_OBJECT

private:
    QNetworkAccessManager *manager;
    QList<SocketManager *> $sockets;

public:

    explicit Net(QObject *parent = nullptr);

    ~Net() override {
        for (int i = 0; i < $sockets.count(); i++) {
            delete $sockets.at(i);
        }
    }

    // host: example.com, port: 443, path: /, isSSL: true
    QString GetSynchronous(const QString &host, int port, const QString &path, bool isSSL);

    QJsonDocument GetJSON(const QString &host, int port, const QString &path, bool isSSL);

    static QByteArray get(const QString &url);


private:

    SocketManager *getSocketManager(const QString &host, int port, bool isSSL);

};

#endif //BILAI_SUITE_NET_CPP
