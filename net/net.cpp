#include "net.h"

Net::Net(QObject *parent) {
    manager = new QNetworkAccessManager(this);
}

// host: example.com, port: 443, path: /, isSSL: true
QString Net::GetSynchronous(const QString &host, int port, const QString &path, bool isSSL) {
    return getSocketManager(host, port, isSSL)->get(path);
}

QJsonDocument Net::GetJSON(const QString &host, int port, const QString &path, bool isSSL) {
    return QJsonDocument::fromJson(getSocketManager(host, port, isSSL)->get(path).toUtf8());
}

SocketManager *Net::getSocketManager(const QString &host, int port, bool isSSL) {

    SocketManager *socketManager = nullptr;

    for (int i = 0; i < $sockets.count(); i++) {
        SocketManager *sm = $sockets.at(i);
        if (sm->host() == host && sm->port() == port && sm->isSSL() == isSSL) {
            socketManager = sm;
            break;
        }
    }

    if (!socketManager) {
        socketManager = new SocketManager(this, host, port, isSSL);
        $sockets.append(socketManager);
    }

    return socketManager;
}

QByteArray Net::get(const QString &url) {

    auto request = QNetworkRequest(QUrl(url));

    QEventLoop synchronizer;
    QNetworkAccessManager man;

    connect(&man, &QNetworkAccessManager::finished, &synchronizer, &QEventLoop::quit);

    QNetworkReply *reply = man.get(request);

    synchronizer.exec();

    if (reply) {
        QByteArray data = reply->readAll();
        reply->deleteLater();
        return data;
    }

    return "";
}
