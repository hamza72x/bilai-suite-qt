//
// Created by Yo on 16/4/21.
//

#include "socket_manager.h"


SocketManager::SocketManager(QObject *parent, const QString &host, int port, bool isSSL) {

    $host = host;
    $port = port;
    $isSSL = isSSL;

    if ($isSSL) {
        $sslSocket = new QSslSocket(this);
    } else {
        $tcpSocket = new QTcpSocket(this);
    }
}

QAbstractSocket *SocketManager::getSocket() const {
    if ($isSSL) return $sslSocket;
    return $tcpSocket;
}

QString SocketManager::host() {
    return $host;
}

int SocketManager::port() const {
    return $port;
}

bool SocketManager::isSSL() const {
    return $isSSL;
}

QString SocketManager::get(const QString &path) {

    this->connectCheck();

    QByteArray reqData("GET ");

    QAbstractSocket *socket = getSocket();

    reqData.append(path.toUtf8());
    reqData.append(" HTTP/1.1\r\nHost: ");
    reqData.append($host.toUtf8());
    reqData.append("\r\n\r\n");

    if (socket->waitForConnected(10000)) {

        socket->write(reqData);
        socket->waitForBytesWritten();

        if (socket->waitForReadyRead(10000)) {
            auto all = QString(socket->readAll());
            logs Qt::endl << path << all << Qt::endl;
            return all.section("\r\n\r\n", 1);
        }

    } else {
        logs "socket->errorString()" << socket->errorString();
    }

    return "";
}

void SocketManager::connectCheck() {
    if (!getSocket()->isOpen()) {
        logs "not open" << $host << $port;
        if ($isSSL) {
            $sslSocket->ignoreSslErrors();
            $sslSocket->connectToHostEncrypted($host, $port);
        } else {
            $tcpSocket->connectToHost($host, $port);
        }
    } else {
        logs "yap open" << $host << $port;
    }
}
