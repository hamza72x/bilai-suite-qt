//
// Created by Yo on 23/4/21.
//

#ifndef BILAI_SUITE_REQUEST_H
#define BILAI_SUITE_REQUEST_H

#include <QObject>

struct Request {

public:
    uint id = -1;
    // in case of modified request
    uint parentRequestId = -1;
    bool isThisModifiedRequest = false;
    bool isSsl = false;
    QString host;
    QString path;
    QString method;
    QString header;
    QString body;
    QString response;
};


#endif //BILAI_SUITE_REQUEST_H
