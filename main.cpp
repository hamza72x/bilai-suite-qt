#include "pages/main_window/main_window.h"

#include <QApplication>
#include <QFontDatabase>
#include "pages/test_widget/test_widget.h"
#include "bilai.h"

int main(int argc, char *argv[]) {

    BILAI::boot();

#ifdef Q_OS_WIN
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling, true);
#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
        QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::PassThrough);
#endif
#endif
QGuiApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
// In Windows, there is no output to terminal for a graphical application, so we install
// the output to message box, until the main window is shown or the application exits.
qInstallMessageHandler(boxMessageOutput);
#endif


    QCoreApplication::setApplicationName("Bilai Suite");
    QCoreApplication::setApplicationVersion("0.1");

    QApplication a(argc, argv);

    a.setStyleSheet(FileUtil::getFileString(":/assets/qss/style.qss"));

    QList<QString> fonts = {"OpenSans-Regular", "OpenSans-Bold", "OpenSans-Italic"};

    for (int i = 0; i < fonts.count(); i++) {
        int id = QFontDatabase::addApplicationFont(":/assets/fonts/" + fonts.at(i) + ".ttf");
        QStringList families = QFontDatabase::applicationFontFamilies(id);
        if (!families.isEmpty()) {
            families.at(0);
        }
    }

    MainWindow w;

//    TestWidget w;

    w.show();

    return a.exec();
}
