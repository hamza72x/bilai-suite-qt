#include "proxy.h"
#include "../bilai.h"

Proxy::Proxy(QObject *parent) : QObject(parent) {
    if (BILAI::IS_DEBUG) {
        $proxyProcessPath = "/Users/nix/code/go-xc/src/gitlab.com/hamza02x/bilai-proxy/bilai-proxy";
    }
    $process = new QProcess(this);
}

Proxy::~Proxy() {
    $process->close();
}

// setters and getters
int Proxy::proxyPort() const {
    return $proxyPort;
}

void Proxy::setProxyPort(int proxyPort) {
    $proxyPort = proxyPort;
}

int Proxy::communicationPort() const {
    return $communicationPort;
}

void Proxy::setCommunicationPort(int communicationPort) {
    $communicationPort = communicationPort;
}

// public methods

void Proxy::start() {
    if (this->isRunning()) {
        return;
    }

    QStringList arguments;

    arguments << "-proxy-port" << QString::number($proxyPort);
    arguments << "-communication-port" << QString::number($communicationPort);
    arguments << "-db-path" << FileUtil::getDBPath();

    logs $proxyProcessPath << arguments;

    connect($process, &QProcess::started, this, &Proxy::started);
    connect($process, &QProcess::errorOccurred, this, &Proxy::errorOccurred);
    connect($process, &QProcess::readyReadStandardError, this, &Proxy::readyReadStandardError);
    connect($process, &QProcess::readyReadStandardOutput, this, &Proxy::readyReadStandardOutput);
    connect($process, &QProcess::stateChanged, this, &Proxy::stateChanged);

    $process->start($proxyProcessPath, arguments);
}

void Proxy::restart() {
    if (this->isRunning()) {
        $process->close();
    }

    this->start();
}

/************************* private funcs *************************/

bool Proxy::isRunning() {
    int state = $process->state();

    return (state == QProcess::Running || state == QProcess::Starting);
}

// private slots
void Proxy::errorOccurred(QProcess::ProcessError error) {
    logs "errorOccurred" << error;
}

void Proxy::finished(int exitCode, QProcess::ExitStatus exitStatus) {
    logs "Finished" << "exitCode" << exitCode << "exitStatus" << exitStatus;
}

void Proxy::readyReadStandardError() {
    logs "readyReadStandardError" << $process->readAllStandardError();
}

void Proxy::readyReadStandardOutput() {
    // logs "readyReadStandardOutput" << $process->readAllStandardOutput();
    logs "readyReadStandardOutput" << $process->readAllStandardOutput().size();
}

void Proxy::started() {
    logs "started";
}

void Proxy::stateChanged(QProcess::ProcessState newState) {
    logs "stateChanged" << newState;
}

void Proxy::stop() {
    if (isRunning()) {
        $process->kill();
        $process->close();
    }
}
