#ifndef PROXY_H
#define PROXY_H

#include <QObject>
#include <QtConcurrent/QtConcurrent>
#include <QProcess>
#include "../file_util/file_util.h"
#include "../logs.h"

class Proxy : public QObject {
Q_OBJECT

private:
    int $proxyPort = 8080;
    int $communicationPort = 8787;

    QProcess *$process;
    QString $proxyProcessPath;


public:
    explicit Proxy(QObject *parent = nullptr);

    ~Proxy() override;

    void start();

    void stop();

    void restart();

    int proxyPort() const;

    void setProxyPort(int proxyPort);

    int communicationPort() const;

    void setCommunicationPort(int communicationPort);

private slots:

    void errorOccurred(QProcess::ProcessError error);

    void finished(int exitCode, QProcess::ExitStatus exitStatus);

    void readyReadStandardError();

    void readyReadStandardOutput();

    void started();

    void stateChanged(QProcess::ProcessState newState);

private:
    bool isRunning();


};

#endif // PROXY_H
