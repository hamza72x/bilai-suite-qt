#ifndef BILAI_SUITE_PAGE_PROXY
#define BILAI_SUITE_PAGE_PROXY

#include <QWidget>
#include <QVBoxLayout>
#include <QStackedWidget>
#include "http_history/table/table_http_history.h"
#include "http_history/http_history_ui.h"
#include "../../components/tab_widget/tab_widget.h"
#include "../test_widget/test_widget.h"

class PageProxy : public QWidget {

Q_OBJECT

private:
    QStackedWidget *$stackWidget;
    TabWidget *tabWidget;

public:
    explicit PageProxy(QWidget *parent = nullptr);

private slots:

    void onTabWidgetClick(int index);
};

#endif