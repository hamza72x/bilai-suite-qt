#include "http_history_ui.h"

HttpHistoryUI::HttpHistoryUI(QWidget *parent) : QWidget(parent) {

    connect(
        new QShortcut(QKeySequence("ctrl+f"), this),
        &QShortcut::activated,
        this,
        &HttpHistoryUI::_onKeyCtrlF
    );

    connect(
        new QShortcut(QKeySequence("esc"), this),
        &QShortcut::activated,
        this,
        &HttpHistoryUI::_onKeyEsc
    );

    // base ui
    this->setObjectName("HttpHistoryUI");
    this->setContentsMargins(5, 10, 5, 10);

    // table customize
    $table = new TableHttpHistory;

    // main layout
    auto *vertical = new QVBoxLayout;

    $filterUi = new FilterUI;

    vertical->addWidget($filterUi);

    // between $table and request_response
    auto *vSplitter = new QSplitter(Qt::Vertical);

    vSplitter->addWidget($table);

    // between request http_view and response http_view
    auto *hSplitter = new QSplitter(Qt::Horizontal);

    $requestUi = new HttpView(HttpView::Type::Request);
    $responseUi = new HttpView(HttpView::Type::Request);

    hSplitter->setHandleWidth(5);

    hSplitter->addWidget($requestUi);
    hSplitter->addWidget($responseUi);

    vSplitter->setContentsMargins(5, 5, 5, 5);
    vSplitter->setHandleWidth(10);

    vSplitter->addWidget(hSplitter);

    vertical->setMargin(0);
    vertical->setSpacing(0);

    vertical->addWidget(vSplitter);

    setLayout(vertical);

    connect($table, &TableHttpHistory::sigOnRowSelection, this, &HttpHistoryUI::_onRowSelection);
    connect($filterUi, &FilterUI::sigOnSearchText, [=](const QString &text) {
        $table->setFilter(text);
    });

}

void HttpHistoryUI::resizeEvent(QResizeEvent *event) {
    $table->setColumnWidth(0, 50); // TODO:- make changeable with max ID size's width
    $table->setColumnWidth(1, 250);
    $table->setColumnWidth(3, 80);
    $table->setColumnWidth(3, this->width() / 3 - 30);
    $table->setColumnWidth(5, 100);
    $table->setColumnWidth(6, 120);
    QWidget::resizeEvent(event);
}

void HttpHistoryUI::_onRowSelection(int requestId) {

    if (requestId == -1) {
        logs "invalid requestId" << requestId;
        return;
    }

    QString url = "http://127.0.0.1:8787/format_by_id?request_id=" + QString::number(requestId);

    $requestUi->setHTML(Net::get(url));
    $responseUi->setHTML(Net::get(url + "&part=response"));
}

/************************* keyboard shortcuts *************************/

void HttpHistoryUI::_onKeyCtrlF() {

    if ($requestUi->searchUi->lineEdit->hasFocus()
        || $responseUi->searchUi->lineEdit->hasFocus()
        || $filterUi->lineEdit->hasFocus()) {
        return;
    }

    if ($requestUi->codeEditor->hasFocus()) {
        $requestUi->searchUi->show();
        $requestUi->searchUi->lineEdit->setFocus();
    } else if ($responseUi->codeEditor->hasFocus()) {
        $responseUi->searchUi->show();
        $responseUi->searchUi->lineEdit->setFocus();
    } else {
        $filterUi->lineEdit->setFocus();
    }

}

void HttpHistoryUI::_onKeyEsc() {
    if ($requestUi->codeEditor->hasFocus() || $requestUi->searchUi->lineEdit->hasFocus()) {
        $requestUi->searchUi->hide();
        $requestUi->codeEditor->setFocus();
    } else if ($responseUi->codeEditor->hasFocus() || $responseUi->searchUi->lineEdit->hasFocus()) {
        $responseUi->searchUi->hide();
        $responseUi->codeEditor->setFocus();
    }
}
