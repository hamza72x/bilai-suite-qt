#ifndef BILAI_SUITE_HTTP_HISTORY_UI
#define BILAI_SUITE_HTTP_HISTORY_UI

#include <QVBoxLayout>
#include <QTableView>
#include <QHeaderView>
#include <QStandardItemModel>
#include <QTimer>
#include <QSplitter>
#include <QtConcurrent/QtConcurrent>
#include <QItemSelectionModel>
#include <QShortcut>
#include <QKeySequence>
#include "table/table_http_history.h"
#include "filter/filter_ui.h"
#include "../../../components/http_view/http_view.h"
#include "../../../net/net.h"

class HttpHistoryUI : public QWidget {

Q_OBJECT

private:
    TableHttpHistory *$table;
    FilterUI *$filterUi;
    HttpView *$requestUi;
    HttpView *$responseUi;

public:
    explicit HttpHistoryUI(QWidget *parent = nullptr);

private slots:

    void _onKeyCtrlF();
    void _onKeyEsc();
    void _onRowSelection(int requestId);

protected:
    void resizeEvent(QResizeEvent *event) override;
};

#endif