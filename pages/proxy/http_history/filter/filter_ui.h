//
// Created by Yo on 29/4/21.
//

#ifndef BILAI_SUITE_FILTER_UI_H
#define BILAI_SUITE_FILTER_UI_H

#include <QWidget>
#include <QAction>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QTimer>
#include "../../../../components/the_button_v2/the_button_v2.h"

class FilterUI : public QWidget {

    Q_OBJECT

private:
    QTimer *$timerEdited;
    QString $searchedText = "";
    bool $isTyping;

public:
    QLineEdit *lineEdit;

public:
    explicit FilterUI(QWidget *parent = nullptr);

signals:

    void sigOnSearchText(const QString &text);

};


#endif //BILAI_SUITE_FILTER_UI_H
