#include "filter_ui.h"

FilterUI::FilterUI(QWidget *parent) : QWidget(parent) {

    $timerEdited = new QTimer(this);
    $timerEdited->setSingleShot(true);

    this->setContentsMargins(5, 0, 0, 10);

    auto *horizontal = new QHBoxLayout;

    horizontal->setMargin(0);
    horizontal->setSpacing(0);

    auto *btnFilter = new TheButtonV2("Filter", ":/assets/images/page-proxy/filter.png", this);
    btnFilter->setImageColor("#415262");

    lineEdit = new QLineEdit;

    lineEdit->setObjectName("QLineEditFilter");

    lineEdit->setClearButtonEnabled(true);
    lineEdit->setAttribute(Qt::WA_MacShowFocusRect, false);
    lineEdit->setPlaceholderText("Search");
    lineEdit->setFixedHeight(30);

    lineEdit->addAction(
        new QAction(QIcon(":/assets/images/page-proxy/search.png"), "", lineEdit),
        QLineEdit::LeadingPosition
    );

    horizontal->addWidget(btnFilter);
    horizontal->addSpacing(10);
    horizontal->addWidget(lineEdit);
    horizontal->addStretch();

    setLayout(horizontal);

    connect($timerEdited, &QTimer::timeout, [this]() {
        if (this->$isTyping) return;
        this->sigOnSearchText(this->$searchedText);
    });

    connect(lineEdit, &QLineEdit::textChanged, [this](const QString& text) {
        this->$isTyping = true;
        this->$searchedText = text;
        this->$timerEdited->start(300);
        this->$isTyping = false;
    });
}

