//
// Created by Yo on 30/4/21.
//

#include "table_http_history.h"

TableHttpHistory::TableHttpHistory(QWidget *parent) : QTableView(parent) {

    this->setObjectName("TableHttpHistory");

    this->setSelectionMode(QAbstractItemView::SingleSelection);
    this->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->verticalHeader()->setVisible(false);
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    this->setShowGrid(false);
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);

    $databaseCheckTimer = new QTimer(this);
    $databaseCheckTimer->setInterval(300);

    $sqlTableModel = new QSqlTableModel;
    $sqlTableModel->setTable("requests");
    $sqlTableModel->setFilter($defaultFilters);
    $sqlTableModel->setSort(0, Qt::DescendingOrder);
    $sqlTableModel->select();

    this->setModel($sqlTableModel);

    // column names
    QSqlQuery query("PRAGMA table_info('requests')", $sqlTableModel->database());

    while (query.next()) {
        QString colName = query.value(1).toString();
        if (!$visibleColumns.contains(colName)) {
            int index = $sqlTableModel->fieldIndex(colName);
            if (index) {
                $sqlTableModel->removeColumn(index);
            }
        }
    }

    for (int i = 0; i < $visibleColumns.count(); i++) {
        QString colName = $visibleColumns.at(i);
        int index = $sqlTableModel->fieldIndex(colName);
        if (index) {
            $sqlTableModel->setHeaderData(index, Qt::Horizontal, colName.toUpper().replace("_", " "));
        }
    }

    connect(horizontalHeader(), &QHeaderView::sectionClicked, this, &TableHttpHistory::$onHeaderClick);
    connect(this, &QTableView::clicked, this, &TableHttpHistory::$onRowSelection);
    connect($databaseCheckTimer, &QTimer::timeout, this, &TableHttpHistory::$onDatabaseCheck);

    $databaseCheckTimer->start();
}

void TableHttpHistory::setFilter(const QString &text) {
    if (text.isEmpty()) {

        $sqlTableModel->setFilter($defaultFilters);

    } else {

        QSqlField f(QLatin1String(""), QVariant::String);
        f.setValue("%" + text + "%");
        QString value = $sqlTableModel->database().driver()->formatValue(f);

        $sqlTableModel->setFilter(
            $defaultFilters +
            " and (host like " + value
            + " or path like " + value
            + " or body like " + value
            + " or header like " + value
            + " or response like " + value
            + ")"
        );
    }

    $sqlTableModel->select();
}

void TableHttpHistory::$onHeaderClick(int columnIndex) {
    logs "headerClick" << columnIndex;
    $sqlTableModel->setSort(columnIndex,
                            $isSortDesc ? Qt::SortOrder::AscendingOrder : Qt::SortOrder::DescendingOrder);
    $isSortDesc = !$isSortDesc;
    $sqlTableModel->select();
}

TableHttpHistory::~TableHttpHistory() {
    $databaseCheckTimer->stop();
}

void TableHttpHistory::$onRowSelection(const QModelIndex &index) {
    sigOnRowSelection($sqlTableModel->record(index.row()).value("id").toInt());
}

void TableHttpHistory::$onDatabaseCheck() {

    while ($sqlTableModel->canFetchMore()) $sqlTableModel->fetchMore();
    int newCount = $getRequestsCount();
    if (newCount > $requestsCount) {
        $sqlTableModel->insertRows(0, newCount - $requestsCount);
        $requestsCount = newCount;
        $sqlTableModel->select();
    }
}

int TableHttpHistory::$getRequestsCount() {
    int count = 0;
    QSqlQuery query("select count(*) from requests", $sqlTableModel->database());
    query.exec();
    while (query.next()) {
        count = query.value(0).toInt();
    }
    return count;
}

//QStringList TableHttpHistory::getColumnNamesUppercase() {
//    QStringList list;
//    for (int i = 0; i < columnNames.count(); ++i) {
//        list << columnNames.at(i).toUpper().replace("_", " ");
//    }
//    return list;
//}

/*
 *


 while (query.next()) {
        r.id = query.value("id").toInt();
        r.host = query.value("host").toString();
        r.path = query.value("path").toString();
        r.method = query.value("method").toString();
        r.body = query.value("body").toString();
        r.isSsl = query.value("is_ssl").toBool();
        r.parentRequestId = query.value("parent_request_id").toInt();
        r.response = query.value("raw_response").toString();
    }

    QSqlQuery queryHeader("select * from request_headers where request_id = ? limit 1", db);
    queryHeader.bindValue(0, r.id);
    queryHeader.exec();

    while (queryHeader.next()) {
        r.header = queryHeader.value("raw").toString();
    }
 */

/*

int DBHistory::getRequestsCount() {


}

 */