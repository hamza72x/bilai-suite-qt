//
// Created by Yo on 30/4/21.
//

#ifndef BILAI_SUITE_TABLE_HTTP_HISTORY_H
#define BILAI_SUITE_TABLE_HTTP_HISTORY_H

#include <QTableView>
#include <QHeaderView>
#include <QScrollBar>
#include "../../../../components/box_shadow/box_shadow.h"
#include "../../../../logs.h"
#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlField>
#include <QtSql/QSqlDriver>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QTimer>

class TableHttpHistory : public QTableView {

Q_OBJECT

private:
    QTimer *$databaseCheckTimer;
    bool $isSortDesc = true;
    int $requestsCount = 0;

    QStringList $visibleColumns = {
        "id", "host", "method", "path",
        "status", "length", "type",
    };

    QSqlTableModel *$sqlTableModel;

    QString $defaultFilters = "(type != 'IMAGE' and type != 'BINARY' and type != 'FONT')";
    /************************* public methods *************************/

public:
    explicit TableHttpHistory(QWidget *parent = nullptr);

    void setFilter(const QString &text);

    ~TableHttpHistory() override;

signals:

    void sigOnRowSelection(int requestId);

    /************************* private methods *************************/

private:
    int $getRequestsCount();

private slots:

    void $onRowSelection(const QModelIndex &index);

    void $onHeaderClick(int columnIndex);

    void $onDatabaseCheck();

};

#endif //BILAI_SUITE_TABLE_HTTP_HISTORY_H
