#include "page_proxy.h"

PageProxy::PageProxy(QWidget *parent) : QWidget(parent) {

    this->setObjectName("PageProxy");

    auto *vertical = new QVBoxLayout;
    vertical->setMargin(0);
    vertical->setSpacing(0);

    auto *httpHistory = new HttpHistoryUI;
    tabWidget = new TabWidget;

    $stackWidget = new QStackedWidget;
    $stackWidget->addWidget(httpHistory);
    $stackWidget->addWidget(new TestWidget);

    vertical->addWidget(tabWidget);
    vertical->addWidget($stackWidget);

    this->setLayout(vertical);

    // signals and slots
    connect(tabWidget, &TabWidget::sigClick, this, &PageProxy::onTabWidgetClick);
}

void PageProxy::onTabWidgetClick(int index) {
    $stackWidget->setCurrentIndex(index);
}
