#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QStringList>
#include <QWidget>
#include <QVBoxLayout>
#include <QSpacerItem>
#include <QCloseEvent>
#include "../../proxy/proxy.h"
#include "../../logs.h"
#include "../../model/request.h"
#include "../../components/buttons_main/buttons_main.h"
#include "../proxy/http_history/http_history_ui.h"
#include "../../components/tab_widget/tab_widget.h"
#include "../../components/border.h"
#include "../proxy/page_proxy.h"

class MainWindow : public QWidget {
Q_OBJECT

private:
    Proxy *$proxy;

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow() override;

protected:

    void closeEvent(QCloseEvent *event) override;

};

#endif // MAIN_WINDOW_H
