#include "main_window.h"

MainWindow::MainWindow(QWidget *parent) : QWidget(parent) {

    $proxy = new Proxy(this);

    this->resize(710, 810);
    this->setWindowTitle("Proxy | Bilai Suite");
    this->setObjectName("MainWindow");

    // layout
    auto *vertical = new QVBoxLayout;

    vertical->setMargin(0);
    vertical->setSpacing(0);

    vertical->addWidget(new ButtonsMain);
    vertical->addWidget(new Border(QColor("#C5C6C5"), 1));
    vertical->addWidget(new PageProxy());

    setLayout(vertical);

    $proxy->start();
}

/************************* OVERRIDES *************************/

void MainWindow::closeEvent(QCloseEvent *event) {
    event->accept();
}

MainWindow::~MainWindow() {
    $proxy->stop();
}
