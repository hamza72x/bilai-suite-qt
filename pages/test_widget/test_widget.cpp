//
// Created by Yo on 29/4/21.
//

#include "test_widget.h"


TestWidget::TestWidget(QWidget *parent) : QWidget(parent) {
    // resize(500, 500);
    // auto *db = new Database(this);

    auto *vertical = new QVBoxLayout;
    auto *le = new QLineEdit;

    QPixmap pixmap(100,100);
    pixmap.fill(QColor("white"));
    QIcon redIcon(pixmap);

    le->setStyleSheet("color: #ffffff");
    le->addAction(new QAction(redIcon, "GG"),
                  QLineEdit::TrailingPosition);


    vertical->addWidget(le);

    setLayout(vertical);
}

void TestWidget::paintEvent(QPaintEvent *event) {
    QWidget::paintEvent(event);

}
