//
// Created by Yo on 29/4/21.
//

#ifndef BILAI_SUITE_TEST_WIDGET_H
#define BILAI_SUITE_TEST_WIDGET_H

#include <QWidget>
#include <QPainter>
#include <QPainterPath>
#include <QColor>
#include <QLineEdit>
#include <QAction>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPointF>
#include <QtSql/QSqlDatabase>
#include "../../logs.h"

class TestWidget : public QWidget {

Q_OBJECT

private:
    QColor red = QColor("red");
    QColor white = QColor("white");
    QColor blue = QColor("blue");

public:
    explicit TestWidget(QWidget *parent = nullptr);

protected:
    void paintEvent(QPaintEvent *event) override;

};

#endif //BILAI_SUITE_TEST_WIDGET_H
