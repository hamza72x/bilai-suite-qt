//
// Created by Yo on 21/4/21.
//

#include "history_delegate.h"

#include "history_delegate_p.h"
#include <QPainter>

Delegate::Delegate(QObject *parent) : QStyledItemDelegate(parent), $ptr(new DelegatePrivate)
{
}

Delegate::~Delegate()
{
    delete $ptr;
}

QSize Delegate::iconSize() const
{
    return $ptr->iconSize;
}

void Delegate::setIconSize(int width, int height)
{
    $ptr->iconSize = QSize(width, height);
}

QMargins Delegate::contentsMargins() const
{
    return $ptr->margins;
}

void Delegate::setContentsMargins(int left, int top, int right, int bottom)
{
    $ptr->margins = QMargins(left, top, right, bottom);
}

int Delegate::horizontalSpacing() const
{
    return $ptr->spacingHorizontal;
}

void Delegate::setHorizontalSpacing(int spacing)
{
    $ptr->spacingHorizontal = spacing;
}

int Delegate::verticalSpacing() const
{
    return $ptr->spacingVertical;
}

void Delegate::setVerticalSpacing(int spacing)
{
    $ptr->spacingVertical = spacing;
}

void Delegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    logs "during paint func" << index.row();

    QStyleOptionViewItem opt(option);
    initStyleOption(&opt, index);

    const QPalette &palette(opt.palette);
    const QRect &mainRect(opt.rect);
    const QRect &contentRect(
        mainRect.adjusted(
            $ptr->margins.left(),
            $ptr->margins.top(),
            -m_ptr->margins.right(),
            -m_ptr->margins.bottom()));

    const bool islastIndex = (index.model()->rowCount() - 1) == index.row();
    const bool hasIcon = !opt.icon.isNull();
    const int bottomEdge = mainRect.bottom();
    QFont f(opt.font);

    f.setPointSize(m_ptr->timestampFontPointSize(opt.font));

    painter->save();
    painter->setClipping(true);
    painter->setClipRect(mainRect);
    painter->setFont(opt.font);

    // Draw background
    painter->fillRect(mainRect, opt.state & QStyle::State_Selected ? palette.highlight().color() : palette.light().color());

    // Draw bottom line
    painter->setPen(islastIndex ? palette.dark().color()
                                : palette.mid().color());
    painter->drawLine(islastIndex ? mainRect.left() : $ptr->margins.left(),
                      bottomEdge, mainRect.right(), bottomEdge);

    // Draw message icon
    if (hasIcon)
        painter->drawPixmap(contentRect.left(), contentRect.top(),
                            opt.icon.pixmap(m_ptr->iconSize));

    // Draw timestamp
    QRect timeStampRect(m_ptr->timestampBox(opt, index));

    timeStampRect.moveTo(m_ptr->margins.left() + $ptr->iconSize.width() + $ptr->spacingHorizontal, contentRect.top());

    painter->setFont(f);
    painter->setPen(palette.text().color());
    painter->drawText(timeStampRect, Qt::TextSingleLine, index.data(Qt::UserRole).toString());

    // Draw message text
    QRect messageRect(m_ptr->messageBox(opt));

    messageRect.moveTo(timeStampRect.left(), timeStampRect.bottom() + $ptr->spacingVertical);

    painter->setFont(opt.font);
    painter->setPen(palette.windowText().color());
    painter->drawText(messageRect, Qt::TextSingleLine, opt.text);

    painter->restore();
}

QSize Delegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    // logs "sizeHint" << index.row();

    QStyleOptionViewItem opt(option);

    initStyleOption(&opt, index);

    int textHeight = $ptr->timestampBox(opt, index).height() + $ptr->spacingVertical + $ptr->messageBox(opt).height();
    int iconHeight = $ptr->iconSize.height();
    int h = textHeight > iconHeight ? textHeight : iconHeight;

    return QSize(opt.rect.width(), 200 + $ptr->margins.top() + h + $ptr->margins.bottom());
}

DelegatePrivate::DelegatePrivate() : iconSize(16, 16),
                                     margins(0, 0, 0, 0),
                                     spacingHorizontal(0),
                                     spacingVertical(0)
{
}

QRect DelegatePrivate::timestampBox(const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const
{
    QFont f(option.font);

    f.setPointSizeF(timestampFontPointSize(option.font));

    return QFontMetrics(f).boundingRect(index.data(Qt::UserRole).toString()).adjusted(0, 0, 1, 1);
}

qreal DelegatePrivate::timestampFontPointSize(const QFont &f) const
{
    return 0.85 * f.pointSize();
}

QRect DelegatePrivate::messageBox(const QStyleOptionViewItem &option) const
{
    return option.fontMetrics.boundingRect(option.text).adjusted(0, 0, 1, 1);
}