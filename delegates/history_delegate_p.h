//
// Created by Yo on 21/4/21.
//

#ifndef BILAI_SUITE_HISTORY_DELEGATE_P_H
#define BILAI_SUITE_HISTORY_DELEGATE_P_H

class DelegatePrivate
{
    DelegatePrivate();

    inline QRect timestampBox(const QStyleOptionViewItem &option,
                              const QModelIndex &index) const;
    inline qreal timestampFontPointSize(const QFont &f) const;
    inline QRect messageBox(const QStyleOptionViewItem &option) const;

    QSize iconSize;
    QMargins margins;
    int spacingHorizontal;
    int spacingVertical;

    friend class Delegate;
};


#endif //BILAI_SUITE_HISTORY_DELEGATE_P_H
