//
// Created by Yo on 21/4/21.
//

#ifndef BILAI_SUITE_HISTORY_DELEGATE_H
#define BILAI_SUITE_HISTORY_DELEGATE_H

#include <QStyledItemDelegate>
#include "../logs.h"

class DelegatePrivate;

/************************* usage *************************/

/* auto *delegate = new Delegate(this);
QPalette p(palette());

p.setBrush(QPalette::WindowText, QColor("#303030"));
p.setBrush(QPalette::Base, QColor("#F0F1F2"));
p.setBrush(QPalette::Light, QColor("#18222D"));
p.setBrush(QPalette::Midlight, QColor("#D3D6D8"));
p.setBrush(QPalette::Mid, QColor("#C5C9Cb"));
p.setBrush(QPalette::Dark, QColor("#9AA0A4"));
p.setBrush(QPalette::Text, QColor("#616b71"));
p.setBrush(QPalette::Highlight, QColor("#E2E4E5"));

delegate->setContentsMargins(8, 8, 8, 8);
delegate->setIconSize(32, 32);
delegate->setHorizontalSpacing(8);
delegate->setVerticalSpacing(4);

ui->listView->setPalette(p);
ui->listView->setModel(new QStandardItemModel(this));
ui->listView->setItemDelegate(delegate);

for (int i = 1; i < 1000; i++) {
    addItem(QString::number(i) + ". This is some text of an info message",
            QPixmap("qrc:/images/icons/48/information.png"),
            QDateTime::currentDateTime());
}
 void MessageList::addMessage(const QString &text, const QPixmap &pixmap,
							 const QDateTime &dateTime)
{
	auto *item = new QStandardItem(QIcon(pixmap), text);

	item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
	item->setData(dateTime.toString("yyyy-MM-dd"), Qt::UserRole);

	static_cast<QStandardItemModel *>(model())->appendRow(item);
	scrollToBottom();
}

void MessageList::clearAll()
{
	static_cast<QStandardItemModel *>(model())->clear();
}

 */
class Delegate : public QStyledItemDelegate {

Q_OBJECT

public:
    explicit Delegate(QObject *parent = nullptr);

    ~Delegate();

    QSize iconSize() const;

    void setIconSize(int width, int height);

    QMargins contentsMargins() const;

    void setContentsMargins(int left, int top, int right, int bottom);

    int horizontalSpacing() const;

    void setHorizontalSpacing(int spacing);

    int verticalSpacing() const;

    void setVerticalSpacing(int spacing);

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;

    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const override;

private:
    DelegatePrivate *$ptr;
};

#endif //BILAI_SUITE_HISTORY_DELEGATE_H
