#include "bilai.h"

bool BILAI::IS_DEBUG;

void BILAI::boot() {

    auto conn = QSqlDatabase::addDatabase("QSQLITE");
    conn.setDatabaseName(FileUtil::getDBPath());

    if (!conn.open()) {
        logs "error opening database" << conn.lastError();
    } else {
        logs "valid db found" << conn;
    }

    IS_DEBUG = true;
}