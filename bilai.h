#ifndef BILAI_SUITE
#define BILAI_SUITE

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include "file_util/file_util.h"

class BILAI {

public:
    static bool IS_DEBUG;
    enum PositionV1 {
        TOP_LEFT = 0x1,
        TOP_RIGHT = 0x2,
        BOTTOM_LEFT = 0x3,
        BOTTOM_RIGHT = 0x4,
    };
    enum PositionV2 {
        TOP = 0x1,
        LEFT = 0x3,
        RIGHT = 0x2,
        BOTTOM = 0x4,
    };
public:
    static void boot();
};


#endif