//
// Created by Yo on 21/4/21.
//

#ifndef BILAI_SUITE_FILE_UTIL_H
#define BILAI_SUITE_FILE_UTIL_H

#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include "../logs.h"

class FileUtil {

private:

public:
    static QString getDBPath() {
        // FIXME: later
        return "/Users/nix/code/qt/bilai-suite/test.db";
    }

    static QString getFileString(const QString &path) {

        QFile f(path);

        if (!f.open(QIODevice::ReadOnly)) {
            logs "Error reading file, path:" << path << f.errorString();
            return "";
        }

        return QTextStream(&f).readAll();
    }
};


#endif //BILAI_SUITE_FILE_UTIL_H
