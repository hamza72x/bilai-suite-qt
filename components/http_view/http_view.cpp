//
// Created by Yo on 5/5/21.
//

#include "http_view.h"

HttpView::HttpView(HttpView::Type type, QWidget *parent) : QWidget(parent) {

    $type = type;

    searchUi = new SearchUI;
    auto *vertical = new QVBoxLayout;

    vertical->setContentsMargins(10, 10, 0, 1);
    vertical->setSpacing(0);

    auto *lblTitle = new QLabel(
        QString("<b>%1<b>").arg(
            $type == HttpView::Request ? "Request" : "Response"
        )
    );

    codeEditor = new CodeEditor;

    vertical->addWidget(lblTitle);
    vertical->addSpacing(5);
    vertical->addWidget(codeEditor);

    auto *horizontal = new QHBoxLayout;
    horizontal->addWidget(searchUi);
    horizontal->addStretch();
    vertical->addLayout(horizontal);
    vertical->addSpacing(10);

    searchUi->hide();

    setLayout(vertical);

    connect(searchUi->btnClose, &QToolButton::clicked, this, &HttpView::_onSearchUiBtnClose);
    connect(searchUi->btnUp, &QToolButton::clicked, this, &HttpView::_onSearchUiBtnUp);
    connect(searchUi->btnDown, &QToolButton::clicked, this, &HttpView::_onSearchUiBtnDown);
    connect(searchUi->lineEdit, &QLineEdit::textChanged, this, &HttpView::_onSearchTextChanged);
    connect(searchUi->lineEdit, &QLineEdit::returnPressed, this, &HttpView::_onSearchTextEnterKey);
}

void HttpView::paintEvent(QPaintEvent *event) {
    QPainterPath path;
    path.addRoundedRect(QRectF(
        1,
        1,
        rect().width() - (1 * 2),
        rect().height() - (1 * 2)
    ), 6, 6);
    QPainter p(this);
    p.strokePath(path, QPen("#949595"));
    QWidget::paintEvent(event);
}

void HttpView::setHTML(const QByteArray &htmlBytes) const {
    codeEditor->setPlainText("");
    codeEditor->appendHtml(QString::fromUtf8(htmlBytes.constData(), htmlBytes.size()));
    codeEditor->moveCursor(QTextCursor::Start);
}

void HttpView::_onSearchUiBtnDown() {
    _find(false);
}

void HttpView::_onSearchUiBtnUp() {
    _find(true);
}

void HttpView::_onSearchTextChanged(const QString &text) {
    $searchText = text;
    _find(false);
}

void HttpView::_find(bool findBackWard) {

    QTextDocument::FindFlags flag;

    if (findBackWard) flag |= QTextDocument::FindBackward;

    // if (casesens) flag |= QTextDocument::FindCaseSensitively;
    // if (words) flag |= QTextDocument::FindWholeWords;

    QTextCursor cursor = codeEditor->textCursor();
    // here , you save the cursor position
    QTextCursor cursorSaved = cursor;

    if (!codeEditor->find($searchText, flag)) {
        //nothing is found | jump to start/end
        cursor.movePosition(findBackWard ? QTextCursor::End : QTextCursor::Start);

        /* following line :
        - the cursor is set at the beginning/end of the document (if search is reverse or not)
        - in the next "find", if the word is found, now you will change the cursor position
        */
        codeEditor->setTextCursor(cursor);

        if (!codeEditor->find($searchText, flag)) {
            //no match in whole document
            // logs ("String not found.");
            // word not found : we set the cursor back to its initial position
            codeEditor->setTextCursor(cursorSaved);
        }
    }
}

void HttpView::_onSearchUiBtnClose() const {
    searchUi->hide();
    codeEditor->setFocus();
}

void HttpView::_onSearchTextEnterKey() {
    _find(false);
}
