//
// Created by Yo on 5/5/21.
//

#ifndef BILAI_SUITE_HTTP_VIEW_H
#define BILAI_SUITE_HTTP_VIEW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPainterPath>
#include <QPainter>
#include <QPaintEvent>
#include <QTextCursor>
#include <QTextEdit>
#include <QTextDocument>
#include <QShortcut>
#include "../code_editor/code_editor.h"
#include "../search/search.h"

class HttpView : public QWidget {

public:

    SearchUI *searchUi;
    CodeEditor *codeEditor;

    enum Type {
        Request, Response
    };

private:
    HttpView::Type $type;
    QString $searchText;

public:
    explicit HttpView(HttpView::Type type, QWidget *parent = nullptr);

    void setHTML(const QByteArray &htmlBytes) const;

    void findNext();

private:
    void _find(bool findBackWard);

private slots:

    void _onSearchUiBtnClose() const;

    void _onSearchUiBtnUp();

    void _onSearchUiBtnDown();

    void _onSearchTextChanged(const QString &text);

    void _onSearchTextEnterKey();

protected:
    void paintEvent(QPaintEvent *event) override;

};

#endif //BILAI_SUITE_HTTP_VIEW_H
