#include "the_button_v2.h"

TheButtonV2::TheButtonV2(
    const QString &text,
    const QString &image,
    QWidget *parent
) : QWidget(parent) {

    $font = QFont("Open Sans", 13);
    $text = text;
    $image = image;
    QFontMetrics fm($font);
    $textWidth = fm.horizontalAdvance(text);
    $textHeight = fm.height();
    $baseWidth = $textWidth + $imageSize + $interMargin + 30;

    setFixedSize(QSize($baseWidth, $baseHeight));

    // $shadow = BoxShadow::ButtonMainTab(this);
}

void TheButtonV2::paintEvent(QPaintEvent *e) {

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    QRect rect = e->rect();
    int widthFull = rect.width();
    int imageX = $baseWidth / 2 - (($imageSize + $interMargin + $textWidth) / 2);

    // background

    auto bgColor = $colBG;

    if ($isPressed) {
        bgColor = "#3D6A97";
    } else if ($isHover) {
        bgColor = "#3D6A97";
    }

    QRectF rectFBackground = QRectF(
        $borderWidth,
        $borderWidth,
        widthFull - ($borderWidth * 2),
        $baseHeight - ($borderWidth * 2)
    );

    QPainterPath pathBackground;
    pathBackground.addRoundedRect(rectFBackground, 6, 6);
    p.fillPath(pathBackground, QColor(bgColor));

    if ($borderWidth > 0) {
        p.strokePath(pathBackground, QPen(QColor($colBorder), $borderWidth));
    }

    // image
    QPixmap px($image);
    QPixmap pxr(px.size());
    pxr.fill($colImage);
    pxr.setMask(px.createMaskFromColor(Qt::transparent));
    p.drawPixmap(
        QRect(
            imageX,
            $baseHeight / 2 - ($imageSize / 2),
            $imageSize,
            $imageSize
        ),
        pxr
    );

    // text
    int textX = imageX + $imageSize + $interMargin;
    p.setPen(QColor("#f1f1f1"));
    p.setFont($font);
    p.drawText(
        QRect(
            textX,
            $baseHeight / 2 - ($textHeight / 2),
            $textWidth,
            $textHeight
        ),
        $text
    );
}

void TheButtonV2::enterEvent(QEvent *event) {
    $isHover = true;
    // $shadow->setBlurRadius(25);
    update();
}

void TheButtonV2::leaveEvent(QEvent *event) {
    $isHover = false;
    // $shadow->setBlurRadius(15);
    update();
}

void TheButtonV2::mousePressEvent(QMouseEvent *e) {
    $isPressed = true;
    update();
}

void TheButtonV2::mouseReleaseEvent(QMouseEvent *e) {
    $isPressed = false;
    update();
    emit sigClick();
}

void TheButtonV2::setBackgroundColor(const QString &color) {
    $colBG = color;
    update();
}

void TheButtonV2::setBorder(const QString &colorBorder, int widthBorder) {
    $colBorder = colorBorder;
    $borderWidth = widthBorder;
    update();
}

void TheButtonV2::setImageColor(const QString &color) {
    $colImage = color;
    update();
}
