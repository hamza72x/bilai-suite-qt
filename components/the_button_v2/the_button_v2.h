#ifndef BILAI_SUITE_THE_BUTTON_V2
#define BILAI_SUITE_THE_BUTTON_V2

#include <QGraphicsDropShadowEffect>
#include <QPixmap>
#include <QBitmap>
#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QPainterPath>
#include <QFont>
#include <QColor>
#include "../box_shadow/box_shadow.h"
#include "../../logs.h"

class TheButtonV2 : public QWidget {

Q_OBJECT

public:

private:
    QString $text;
    QString $image;
    QFont $font;

    QString $colBG = "#18222D";
    QString $colBorder = "#949595";
    QString $colImage = "#35A45A";

    int $borderWidth = 1;

    int $imageSize = 16;
    int $interMargin = 5;
    int $textWidth;
    int $textHeight;

    int $baseWidth;
    int $baseHeight = 30; // row height basically

    bool $isHover = false;
    bool $isPressed = false;

    // QGraphicsDropShadowEffect *$shadow;

public:
    explicit TheButtonV2(
        const QString &text = "",
        const QString &image = "",
        QWidget *parent = nullptr
    );

    void setBackgroundColor(const QString& color);
    void setImageColor(const QString& color);
    void setBorder(const QString& colorBorder, int widthBorder);

protected:
    void paintEvent(QPaintEvent *) override;

    void enterEvent(QEvent *event) override;

    void leaveEvent(QEvent *event) override;

    void mousePressEvent(QMouseEvent *e) override;

    void mouseReleaseEvent(QMouseEvent *e) override;

signals:

    void sigClick();

private:

};

#endif