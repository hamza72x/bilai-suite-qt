#include "tab_widget.h"

TabWidget::TabWidget(QWidget *parent) : QWidget(parent) {

    setObjectName("TabWidget");
    setContentsMargins(0, 0, 0, 0);

    QPalette pal = this->palette();

    pal.setColor(QPalette::Background, QColor("#121415"));
    this->setAutoFillBackground(true);
    this->setPalette(pal);

    auto *layout = new QHBoxLayout(this);

    layout->setSpacing(0);
    layout->setMargin(0);

    QStringList titles = {"History", "Tree View", "Intercept", "Settings"};
    QStringList images = {"list.png", "tree.png", "intercept.png", "settings.png"};

    for (int i = 0; i < titles.count(); i++) {
        auto *btn = new TheButton(
            i,
            0,
            titles.at(i),
            ":/assets/images/page-proxy/" + images.at(i),
            TheButton::ButtonType::Tab,
            this
        );
        $buttons.append(btn);
        connect(btn, &TheButton::sigClick, this, &TabWidget::onClick);
        layout->addWidget(btn);
    }

    layout->addStretch();


}

void TabWidget::onClick(int index) {
    for (int i = 0; i < $buttons.count(); i++) {
        $buttons.at(i)->setActiveIndex(index);
    }
    emit sigClick(index);
}