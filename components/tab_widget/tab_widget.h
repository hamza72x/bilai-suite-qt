#ifndef BILAI_SUITE_TAB_BAR_V1
#define BILAI_SUITE_TAB_BAR_V1

#include <QTabBar>
#include <QStyle>
#include <QPaintEvent>
#include <QStylePainter>
#include <QPushButton>
#include <QHBoxLayout>
#include "../the_button/the_button.h"

class TabWidget : public QWidget {

Q_OBJECT

private:
    QVector<TheButton *> $buttons;

public:
    explicit TabWidget(QWidget *parent = nullptr);

signals:

    void sigClick(int index);

private slots:

    void onClick(int index);
};

#endif