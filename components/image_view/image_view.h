//
// Created by Yo on 29/4/21.
//

#ifndef BILAI_SUITE_IMAGE_VIEW_H
#define BILAI_SUITE_IMAGE_VIEW_H

#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QPen>
#include <QPainterPath>
#include <QBitmap>
#include "../../logs.h"
#include "../../bilai.h"

class ImageView : public QWidget {

Q_OBJECT

private:
    int $imageSize = 20;
    int $cornerRadius = 6;
    QVector<BILAI::PositionV1> $cornerRadiusPositions = {};
    QString $imagePath;
    QString $imageColor;
    QString $colBG = "#18222D";
    QString $colParent = "#18222D";

public:
    explicit ImageView(
        const QString &imagePath,
        const QString &imageColor,
        int imageSize = 20,
        QSize containerSize = QSize(30, 30),
        QWidget *parent = nullptr
    );

    void setBackgroundColor(const QString &color);
    void setCornerRadius(int size, const QVector<BILAI::PositionV1> &positions);

protected:
    void paintEvent(QPaintEvent *event) override;

};


#endif //BILAI_SUITE_IMAGE_VIEW_H
