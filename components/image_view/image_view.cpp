//
// Created by Yo on 29/4/21.
//

#include "image_view.h"

ImageView::ImageView(

    const QString &imagePath,
    const QString &imageColor,
    int imageSize,
    QSize containerSize,
    QWidget *parent

) : QWidget(parent) {

    $imagePath = imagePath;
    $imageColor = imageColor;
    $imageSize = imageSize;

    setFixedSize(containerSize);
}

void ImageView::paintEvent(QPaintEvent *event) {

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);
    QRect rect = event->rect();
    const int width = rect.width();
    const int height = rect.height();
    // background
    QPainterPath pathBG;
    pathBG.addRoundedRect(rect, 0, 0);
    p.fillPath(pathBG, QBrush(QColor($colBG)));

    // image
    QPixmap px($imagePath);
    QPixmap pxr(px.size());
    pxr.fill(QColor($imageColor));
    pxr.setMask(px.createMaskFromColor(Qt::transparent));
    p.drawPixmap(
        QRect(
            width / 2 - ($imageSize / 2),
            height / 2 - ($imageSize / 2),
            $imageSize,
            $imageSize
        ),
        pxr
    );
        // corner radius
    if (!$cornerRadiusPositions.isEmpty()) {

        // x => edge of box
        // y => edge of box

        auto brushBG = QBrush(QColor($colBG));
        auto brushBGParent = QBrush(QColor($colParent));

        if ($cornerRadiusPositions.contains(BILAI::TOP_LEFT)) {

            QPainterPath pathTriangle;
            QPainterPath pathRadius;

            int x = 0;
            int y = 0;

            pathTriangle.moveTo(x, y);
            pathTriangle.lineTo(x, y + $cornerRadius);
            pathTriangle.lineTo(x + $cornerRadius, y);

            p.fillPath(pathTriangle, brushBGParent);

            pathRadius.moveTo($cornerRadius, $cornerRadius);
            pathRadius.arcTo(x, y, $cornerRadius * 2, $cornerRadius * 2, 90, 90);

            p.fillPath(pathRadius, brushBG);
        }
        if ($cornerRadiusPositions.contains(BILAI::TOP_RIGHT)) {

            QPainterPath pathTriangle;
            QPainterPath pathRadius;

            int x = rect.width();
            int y = 0;

            pathTriangle.moveTo(x, y);
            pathTriangle.lineTo(x, y + $cornerRadius);
            pathTriangle.lineTo(x - $cornerRadius, y);

            p.fillPath(pathTriangle, brushBGParent);

            pathRadius.moveTo(x - $cornerRadius, y + $cornerRadius);
            pathRadius.arcTo(x - ($cornerRadius * 2), y, $cornerRadius * 2, $cornerRadius * 2, 0, 90);

            p.fillPath(pathRadius, brushBG);
        }

        if ($cornerRadiusPositions.contains(BILAI::BOTTOM_LEFT)) {

            QPainterPath pathTriangle;
            QPainterPath pathRadius;

            int x = 0;
            int y = rect.height();

            pathTriangle.moveTo(x, y);
            pathTriangle.lineTo(x + $cornerRadius, y);
            pathTriangle.lineTo(x, y - $cornerRadius);

            p.fillPath(pathTriangle, brushBGParent);

            pathRadius.moveTo(x + $cornerRadius, y - $cornerRadius);
            pathRadius.arcTo(x, y - ($cornerRadius * 2), $cornerRadius * 2, $cornerRadius * 2, 180, 90);

            p.fillPath(pathRadius, brushBG);
        }

        if ($cornerRadiusPositions.contains(BILAI::BOTTOM_RIGHT)) {

            QPainterPath pathTriangle;
            QPainterPath pathRadius;

            int y = rect.height();
            int x = rect.width();

            pathTriangle.moveTo(x, y);
            pathTriangle.lineTo(x - $cornerRadius, y);
            pathTriangle.lineTo(x, y - $cornerRadius);

            p.fillPath(pathTriangle, brushBGParent);

            pathRadius.moveTo(x - $cornerRadius, y - $cornerRadius);
            pathRadius.arcTo(x - ($cornerRadius * 2), y - ($cornerRadius * 2), $cornerRadius * 2, $cornerRadius * 2,
                             270, 90);

            p.fillPath(pathRadius, brushBG);
        }
    }
}

void ImageView::setBackgroundColor(const QString &color) {
    $colBG = color;
    update();
}

void ImageView::setCornerRadius(int size, const QVector<BILAI::PositionV1> &positions) {
    $cornerRadius = size;
    $cornerRadiusPositions = positions;
    update();
}