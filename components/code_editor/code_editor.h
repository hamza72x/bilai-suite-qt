//
// Created by Yo on 6/5/21.
//

#ifndef BILAI_SUITE_CODE_EDITOR_H
#define BILAI_SUITE_CODE_EDITOR_H

#include <QWidget>
#include <QPainter>
#include <QPainterPath>
#include <QPainterPath>
#include <QTextBlock>
#include <QContextMenuEvent>
#include <QPlainTextEdit>
#include <QMenu>

class CodeEditor : public QPlainTextEdit {
Q_OBJECT

private:
    bool isFocused = false;
    QWidget *lineNumberArea;

    class LineNumberArea : public QWidget {

    private:
        CodeEditor *codeEditor;

    public:
        explicit LineNumberArea(CodeEditor *editor) : QWidget(editor), codeEditor(editor) {}

        QSize sizeHint() const override {
            return {codeEditor->lineNumberAreaWidth(), 0};
        }

    protected:
        void paintEvent(QPaintEvent *event) override {
            codeEditor->lineNumberAreaPaintEvent(event);
        }

    };

public:
    explicit CodeEditor(QWidget *parent = nullptr);

    void lineNumberAreaPaintEvent(QPaintEvent *event);

    int lineNumberAreaWidth();

protected:
    void resizeEvent(QResizeEvent *event) override;

    void focusInEvent(QFocusEvent *e) override;

    void focusOutEvent(QFocusEvent *e) override;
    void contextMenuEvent(QContextMenuEvent *event) override;

private slots:

    void updateLineNumberAreaWidth(int newBlockCount);

    void highlightCurrentLine();

    void updateLineNumberArea(const QRect &rect, int dy);

};


#endif //BILAI_SUITE_CODE_EDITOR_H
