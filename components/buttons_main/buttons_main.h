//
// Created by Yo on 27/4/21.
//
#ifndef BILAI_SUITE_MAIN_BUTTONS_H
#define BILAI_SUITE_MAIN_BUTTONS_H

#include <QWidget>
#include <QHBoxLayout>
#include <QPalette>
#include <QPushButton>
#include "../box_shadow/box_shadow.h"
#include "../../logs.h"
#include "../the_button/the_button.h"

class ButtonsMain : public QWidget {

Q_OBJECT

private:
    QVector<TheButton *> $buttons;

public:
    explicit ButtonsMain(QWidget *parent = nullptr);

private slots:

    void onClick(int index);

signals:
    void sigClick(int index);
};


#endif //BILAI_SUITE_MAIN_BUTTONS_H
