#include "buttons_main.h"


ButtonsMain::ButtonsMain(QWidget *parent) : QWidget(parent) {

    setContentsMargins(10, 10, 0, 10);
    setObjectName("ButtonsMain");

    QPalette pal = this->palette();

    pal.setColor(QPalette::Background, QColor("#121415"));
    this->setAutoFillBackground(true);
    this->setPalette(pal);

    auto *horizontal = new QHBoxLayout(this);

    horizontal->setMargin(0);
    horizontal->setSpacing(0);

    QStringList titles = {"Target", "Proxy", "Repeater", "Fuzzer", "Analyzer", "Converter"};
    QStringList images = {"target.png",
                          "proxy.png",
                          "repeater.png",
                          "fuzzer.png",
                          "analyzer.png",
                          "converter.png"};

    auto *btnMenu = new QPushButton(this);
    btnMenu->setObjectName("BtnMenu");
    btnMenu->setIcon(QIcon(":/assets/images/menu.png"));
    btnMenu->setIconSize(QSize(20, 20));
    btnMenu->setFixedSize(30, 30);
    horizontal->addWidget(btnMenu);
    horizontal->addSpacing(10);

    for (int i = 0; i < titles.count(); i++) {

        const QString &text = titles.at(i);
        QString image = ":/assets/images/main-tab/" + images.at(i);

        auto *btn = new TheButton(
            i,
            1,
            text,
            image,
            TheButton::ButtonType::V1
        );

        $buttons.append(btn);
        horizontal->addWidget(btn);
        horizontal->addSpacing(15);

        connect(btn, &TheButton::sigClick, this, &ButtonsMain::onClick);
    }

    horizontal->addStretch();
}

void ButtonsMain::onClick(int index) {
    for (int i = 0; i < $buttons.count(); i++) {
        $buttons.at(i)->setActiveIndex(index);
    }
    emit sigClick(index);
}



