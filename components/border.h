#ifndef BILAI_SUITE_BORDER
#define BILAI_SUITE_BORDER

#include <QWidget>

class Border : public QWidget {

Q_OBJECT

public:
    explicit Border(const QColor& color = QColor("red"), int height = 1, QWidget *parent = nullptr) {
        setFixedHeight(height);
        QPalette pal = this->palette();
        pal.setColor(QPalette::Background, color);
        this->setAutoFillBackground(true);
        this->setPalette(pal);
    }
};


#endif