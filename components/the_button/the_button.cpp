#include "the_button.h"

TheButton::TheButton(
    int index,
    int activeIndex,
    const QString &text,
    const QString &image,
    const ButtonType &buttonType,
    QWidget *parent
) : QWidget(parent) {

    this->index = index;

    $animation = new QPropertyAnimation(this);
    $animation->setDuration(200);
    $activeIndex = activeIndex;
    $buttonType = buttonType;

    if (text == "Target" || text == "Converter") {
        $imageSize -= 2;
    }

    $font = QFont("Open Sans", 13);
    $text = text;
    $image = image;
    QFontMetrics fm($font);
    $textWidth = fm.horizontalAdvance(text);
    $textHeight = fm.height();
    $baseWidth = $textWidth + $imageSize + $interMargin + 30;

    setFixedSize(QSize($baseWidth, $baseHeight));

    $shadow = BoxShadow::ButtonMainTab(this);

    if (buttonType == ButtonType::Tab) {
        $shadow->setBlurRadius(0);
    } else {
        $shadow->setBlurRadius($activeIndex == index ? 15 : 0);
    }
}

void TheButton::paintEvent(QPaintEvent *e) {

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    QRect rect = e->rect();
    int imageX = $baseWidth / 2 - (($imageSize + $interMargin + $textWidth) / 2);

    // background

    if ($buttonType == ButtonType::Tab) {

        auto bgColor = QColor("#121415");
        if ($activeIndex == index) {
            bgColor = QColor("#18222D");
        }
        p.fillRect(rect, bgColor);

        if ($activeIndex != -1) {

            int size = 10;
            int y = $baseHeight - size;

            if ($activeIndex + 1 == index) {

                int x = 0;

                QPainterPath pathTriangle;

                pathTriangle.moveTo(x, y);
                pathTriangle.lineTo(x, y + size);
                pathTriangle.lineTo(x + size, y + size);

                p.fillPath(pathTriangle, QBrush("#18222D"));

                QPainterPath pathRadius;
                pathRadius.moveTo(x + size, y);
                pathRadius.arcTo(x, y - size, size * 2, size * 2, 180, 90);

                p.fillPath(pathRadius, QBrush("#121415"));
            } else if ($activeIndex - 1 == index) {

                int x = rect.width() - size;

                QPainterPath pathTriangle;
                pathTriangle.moveTo(x + size, y);
                pathTriangle.lineTo(x + size, y + size);
                pathTriangle.lineTo(x, y + size);

                p.fillPath(pathTriangle, QBrush("#18222D"));

                QPainterPath pathRadius;
                pathRadius.moveTo(x, y);
                pathRadius.arcTo(x - size, y - size, size * 2, size * 2, 270, 90);

                p.fillPath(pathRadius, QBrush("#121415"));

            }

        }

    } else { // BUTTON = V1

        auto bgColor = QColor("#121415");

        if ($activeIndex == index) {
            bgColor = QColor("#18222D");
        } else if ($isPressed) {
            bgColor = QColor("#f3f3f3");
        }

        QPainterPath path;
        path.addRoundedRect(rect, 6, 6);
        p.fillPath(path, bgColor);
    }

    // image
    QPixmap px($image);
    QPixmap pxr(px.size());
    pxr.fill(QColor("#35A45A"));
    pxr.setMask(px.createMaskFromColor(Qt::transparent));
    p.drawPixmap(
        QRect(
            imageX,
            $baseHeight / 2 - ($imageSize / 2),
            $imageSize,
            $imageSize
        ),
        pxr
    );

    // text
    int textX = imageX + $imageSize + $interMargin;
    p.setPen(QColor("#f1f1f1"));
    p.setFont($font);
    p.drawText(
        QRect(
            textX,
            $baseHeight / 2 - ($textHeight / 2),
            $textWidth,
            $textHeight
        ),
        $text
    );

    // badge number

    if ($badgeNumber > 0) {

        QFont fontBadge("Open Sans", 8);
        QFontMetrics f(fontBadge);

        int widthTxtBadge = f.horizontalAdvance(QString::number($badgeNumber));
        int widthBgBadge = widthTxtBadge + 5;

        QRect badgeRect = QRect(
            rect.width() - (widthBgBadge + 10),
            $baseHeight / 2 - (widthBgBadge / 2),
            widthBgBadge,
            widthBgBadge
        );

        QPainterPath path2;
        path2.addRoundedRect(badgeRect, 10, 10);
        p.fillPath(path2, QColor("red"));

        p.setFont(fontBadge);
        p.setPen(QColor("white"));
        p.drawText(badgeRect, Qt::AlignVCenter | Qt::AlignHCenter, QString::number($badgeNumber));
    }
}

void TheButton::enterEvent(QEvent *event) {
    $isHover = true;
    if ($buttonType == ButtonType::V1 && index != $activeIndex) {
        $animation->setTargetObject($shadow);
        $animation->setStartValue(0);
        $animation->setEndValue(25);
        $animation->start();
        connect($animation, &QPropertyAnimation::valueChanged, [=](QVariant value) {
            if ($animation->state() == QPropertyAnimation::Running) {
                $shadow->setBlurRadius(value.toInt());
            }
        });
    }
    update();
}

void TheButton::leaveEvent(QEvent *event) {
    $isHover = false;
    $animation->stop();
    if ($buttonType == ButtonType::V1) {
        $shadow->setBlurRadius($activeIndex == index ? 15 : 0);
    }
    update();
}

void TheButton::mousePressEvent(QMouseEvent *e) {
    $isPressed = true;
    update();
}

void TheButton::mouseReleaseEvent(QMouseEvent *e) {
    $isPressed = false;
    update();
    emit sigClick(index);
}

void TheButton::setBadgeNumber(int number) {
    $badgeNumber = number;
    setFixedWidth(number > 0 ? $baseWidth + $interMargin + 15 : $baseWidth);
    update();
}

void TheButton::setActiveIndex(int activeIndex) {
    $activeIndex = activeIndex;
    if (index != $activeIndex) {
        $shadow->setBlurRadius(0);
    }
    update();
}
