#ifndef BILAI_SUITE_THE_BUTTON_V1
#define BILAI_SUITE_THE_BUTTON_V1

#include <QGraphicsDropShadowEffect>
#include <QPixmap>
#include <QBitmap>
#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QPainterPath>
#include <QFont>
#include <QColor>
#include <QPropertyAnimation>
#include "../box_shadow/box_shadow.h"
#include "../../logs.h"

class TheButton : public QWidget {

Q_OBJECT

public:
    int index;

    enum ButtonType {
        V1, Tab
    };

private:
    QString $text;
    QString $image;
    QFont $font;
    ButtonType $buttonType;
    QPropertyAnimation *$animation;

    int $imageSize = 16;
    int $interMargin = 5;
    int $textWidth;
    int $textHeight;

    int $baseWidth; // without badge number
    int $baseHeight = 30; // row height basically

    bool $isHover = false;
    bool $isPressed = false;
    int $activeIndex = -1;

    int $badgeNumber = 0;

    QGraphicsDropShadowEffect *$shadow;

public:
    explicit TheButton(
        int index = 0,
        int activeIndex = 0,
        const QString &text = "",
        const QString &image = "",
        const ButtonType& buttonType = ButtonType::V1,
        QWidget *parent = nullptr
    );

    void setActiveIndex(int activeIndex);
    void setBadgeNumber(int number);

protected:
    void paintEvent(QPaintEvent *) override;

    void enterEvent(QEvent *event) override;

    void leaveEvent(QEvent *event) override;

    void mousePressEvent(QMouseEvent *e) override;

    void mouseReleaseEvent(QMouseEvent *e) override;

signals:

    void sigClick(int index);

private:

};

#endif