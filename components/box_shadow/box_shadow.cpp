#include "box_shadow.h"

QGraphicsDropShadowEffect* BoxShadow::ButtonMainTab(QWidget *to, int blurRadius) {
    auto *shadow = new QGraphicsDropShadowEffect(to);
    shadow->setBlurRadius(blurRadius);
    shadow->setColor(QColor("#FFFFFF")); // QColor(0, 0, 0, 85)
    shadow->setOffset(QPointF(0, 0));
    to->setGraphicsEffect(shadow);
    return shadow;
}

QGraphicsDropShadowEffect* BoxShadow::V2(QWidget *to) {
    auto *shadow = new QGraphicsDropShadowEffect(to);
    shadow->setBlurRadius(10);
    shadow->setColor(QColor(0, 0, 0, 85));
    shadow->setOffset(QPointF(0, 0));
    to->setGraphicsEffect(shadow);
    return shadow;
}

void BoxShadow::Test(QWidget *to) {
    auto *shadow = new QGraphicsDropShadowEffect;
    shadow->setBlurRadius(20);
    shadow->setColor(QColor("red"));
    shadow->setOffset(QPointF(0, 2));
    to->setGraphicsEffect(shadow);
}

