//
// Created by Yo on 27/4/21.
//

#ifndef BILAI_SUITE_BOX_SHADOW_H
#define BILAI_SUITE_BOX_SHADOW_H

#include <QWidget>
#include <QGraphicsDropShadowEffect>

class BoxShadow {

public:
    static QGraphicsDropShadowEffect *ButtonMainTab(QWidget *to = nullptr, int blurRadius = 15);

    static QGraphicsDropShadowEffect *V2(QWidget *to = nullptr);

    static void Test(QWidget *to = nullptr);

};

#endif //BILAI_SUITE_BOX_SHADOW_H
