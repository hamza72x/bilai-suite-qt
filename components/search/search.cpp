//
// Created by Yo on 16/5/21.
//

#include "search.h"

SearchUI::SearchUI(QWidget *parent) : QWidget(parent) {

    this->setObjectName("SearchUI");
    this->setFixedHeight(30);

    auto *horizontal = new QHBoxLayout;

    horizontal->setMargin(0);
    horizontal->setSpacing(0);

    lineEdit = new QLineEdit;
    lineEdit->setObjectName("QLineEditSearch");
    lineEdit->setFixedHeight(30);
    lineEdit->setAttribute(Qt::WA_MacShowFocusRect, false);
    lineEdit->setPlaceholderText("Search");
    lineEdit->setFixedHeight(height());

    /* auto *lbl = new QLabel("23/23");
    lbl->setObjectName("QLabelSearch");
    lbl->setFont(QFont("Open Sans", 10)); */

    btnUp = new QToolButton;
    btnUp->setObjectName("QToolButtonSearch");
    btnUp->setIcon(QIcon(":/assets/images/arrow-top.png"));

    btnDown = new QToolButton;
    btnDown->setObjectName("QToolButtonSearch");
    btnDown->setIcon(QIcon(":/assets/images/arrow-bottom.png"));

    btnClose = new QToolButton;
    btnClose->setObjectName("QToolButtonSearch");
    btnClose->setIcon(QIcon(":/assets/images/close.png"));

    horizontal->addSpacing(5);
    horizontal->addWidget(lineEdit);
    // horizontal->addWidget(lbl);
    // horizontal->addSpacing(5);
    horizontal->addWidget(btnUp);
    horizontal->addWidget(btnDown);
    horizontal->addWidget(btnClose);
    horizontal->addSpacing(5);

    setLayout(horizontal);
}
