//
// Created by Yo on 16/5/21.
//

#ifndef BILAI_SUITE_SEARCH_H
#define BILAI_SUITE_SEARCH_H

#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QToolButton>
#include <QFontMetrics>
#include <QHBoxLayout>

class SearchUI : public QWidget {

public:
    QLineEdit *lineEdit;
    QToolButton *btnClose;
    QToolButton *btnUp;
    QToolButton *btnDown;

public:
    explicit SearchUI(QWidget *parent = nullptr);
};


#endif //BILAI_SUITE_SEARCH_H
