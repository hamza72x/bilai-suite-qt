//
// Created by Yo on 8/5/21.
//

#ifndef BILAI_SUITE_HIGHLIGHTER_HTTP_H
#define BILAI_SUITE_HIGHLIGHTER_HTTP_H

#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QRegularExpression>

class HighlighterHttp : public QSyntaxHighlighter
{
Q_OBJECT

public:
    explicit HighlighterHttp(QTextDocument *parent = 0);

protected:
    void highlightBlock(const QString &text) override;

private:
    struct HighlightingRule
    {
        QRegularExpression pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> highlightingRules;

    QRegularExpression commentStartExpression;
    QRegularExpression commentEndExpression;

    QTextCharFormat keywordFormat;
    QTextCharFormat classFormat;
    QTextCharFormat singleLineCommentFormat;
    QTextCharFormat multiLineCommentFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat functionFormat;
};


#endif //BILAI_SUITE_HIGHLIGHTER_HTTP_H
